//
//  SearchViewController.swift
//  myTV
//
//  Created by Maurizio Minieri on 06/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //see the walking dead to get an idea of ​​the complete product
   
	 var tvItemArrayJson: [TVItem]!
    var filteredData: [TVItem]!
    var actors: [Actor]!
    var imageName: String!
    var name: String!
    var category: TVItem.Category!
    var genre: [String]!
    var year: String!
    var tvItemDescription: String!
    var during: String!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.becomeFirstResponder()
        filteredData=tvItemArrayJson
        
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField

        textFieldInsideSearchBar?.textColor = .white
       
    }
  
    
    
    //MARK: - CollectionView
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
          
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           
        return filteredData.count
    }
       
       
    //creazione delle celle
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
       // cell.imageView?.clipsToBounds=true
       // cell.imageView?.contentMode = UIView.ContentMode.scaleAspectFill

       // cell.backgroundColor = UIColor.darkGray
        cell.layer.borderColor = #colorLiteral(red: 0.819617033, green: 0.8010364175, blue: 0, alpha: 1) //UIColor.white.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8

        
        cell.titleLabel.text = filteredData[indexPath.row].name
        cell.durationLabel.text = filteredData[indexPath.row].during
        cell.genreLabel.text = filteredData[indexPath.row].genre.joined(separator: " • ")
        cell.imageView.image = UIImage(named: filteredData[indexPath.row].imageName)
        cell.imageView.isUserInteractionEnabled = true
        cell.imageView.tag = indexPath.row

        
        return cell
    }
       
       
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        imageName = filteredData[indexPath.row].imageName
        actors = filteredData[indexPath.row].actors
        category = filteredData[indexPath.row].category
        name = filteredData[indexPath.row].name
        tvItemDescription = filteredData[indexPath.row].description
        year = filteredData[indexPath.row].year
        genre = filteredData[indexPath.row].genre
        during = filteredData[indexPath.row].during

        self.performSegue(withIdentifier: "mySegue", sender: self)
    }
          
       
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         
       if segue.identifier == "mySegue"{
           if let vc = segue.destination as? InfoController {
             
              vc.name = name
              vc.category = category
              vc.year = year
              vc.tvItemDescription = tvItemDescription
              vc.actors = actors
              vc.genre = genre
              vc.during = during
              vc.imageName = imageName
           
           }
       }
     }
       
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
       
       let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

       let numberofItem: CGFloat = 1

       let collectionViewWidth = self.collectionView.bounds.width

       let extraSpace = (numberofItem - 1) * flowLayout.minimumInteritemSpacing

       let inset = flowLayout.sectionInset.right + flowLayout.sectionInset.left

       let width = Int((collectionViewWidth - extraSpace - inset) / numberofItem)

      // print(width)

       return CGSize(width: width, height: 84)
    }

    
    
   //MARK: - SearchBar
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        filteredData = searchText.isEmpty ? tvItemArrayJson : tvItemArrayJson.filter { (item: TVItem) -> Bool in
        // If dataItem matches the searchText, return true to include it
        return item.name.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }

        collectionView.reloadData()
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.showsCancelButton = false
        searchBar.endEditing(true)
        filteredData = tvItemArrayJson
        collectionView.reloadData()
    }
    
   
}
