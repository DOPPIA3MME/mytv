//
//  InfoController.swift
//  myTV
//
//  Created by Maurizio Minieri on 06/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import SwiftUI

class InfoController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var categoryLabel: UILabel!
    
    var name: String!
    var category: TVItem.Category!
    var year: String!
    var actors: [Actor]!
    var genre: [String]!
    var imageName: String!
    var tvItemDescription: String!
    var during: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = UIImage(named: imageName)
        yearLabel.text = year
        genreLabel.text = genre.joined(separator: " • ")
        textView.text = tvItemDescription
        durationLabel.text = during
        titleLabel.text = name
       // categoryLabel.text = category.rawValue
    }
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
             
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{

        return actors.count
    }
          
          
    //creazione delle celle
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
        cell.imageView?.clipsToBounds=true
        cell.imageView?.contentMode = UIView.ContentMode.scaleAspectFill

        cell.backgroundColor = UIColor.green

        cell.realNameActorLabel.text = actors[indexPath.row].realName
        cell.fakeNameActorLabel.text = actors[indexPath.row].fakeName
        cell.imageView.image = UIImage(named: actors[indexPath.row].imageName)
        cell.imageView.isUserInteractionEnabled = true
        cell.imageView.tag = indexPath.row
        
        return cell
    }
    
    
    
    
  //cell click
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        guard let url = URL(string: actors[indexPath.row].link) else { return }
        UIApplication.shared.open(url)
   }
    
    //attributi della cella
    //Controlla il size inspector della collection view lo spazio tra celle
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

        let numberofItem: CGFloat = 2

        let collectionViewWidth = self.collectionView.bounds.width

        let extraSpace = (numberofItem - 1) * flowLayout.minimumInteritemSpacing

        let inset = flowLayout.sectionInset.right + flowLayout.sectionInset.left

        let width = Int((collectionViewWidth - extraSpace - inset) / numberofItem)

        return CGSize(width: width, height: 260)
    }
    
    
 
}
