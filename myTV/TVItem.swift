//
//  TVItem.swift
//  myTV
//
//  Created by Maurizio Minieri on 05/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import UIKit

struct TVItem{
    
    enum Category: String{
        case serie = "serie"
        case movie = "film"
    }

    var name: String
    var category: Category
    var year: String
    var actors: [Actor]
    var genre: [String]
    var imageName: String
    var description: String
    var during: String
    var suggested: Bool
}
