//
//  TabBarController.swift
//  myTV
//
//  Created by Maurizio Minieri on 09/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor.black
    }
    
}
