//
//  Actor.swift
//  myTV
//
//  Created by Maurizio Minieri on 09/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import UIKit

struct Actor{
    
    var realName: String
    var fakeName: String
    var imageName: String
    var link: String
}

