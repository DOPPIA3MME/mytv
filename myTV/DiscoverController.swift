//
//  ViewController.swift
//  myTV
//
//  Created by Maurizio Minieri on 05/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import SwiftUI

class DiscoverController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate, UITabBarDelegate {
    
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
 
	var tvItemArrayJson: [TVItem]!
	var array: [TVItem]!
	var topMovies: [TVItem]!
	var topSeries: [TVItem]!
	var sugSeries: [TVItem]!
	var sugMovies: [TVItem]!
	
	
	var actors: [Actor]!
	var imageName: String!
	var name: String!
	var category: TVItem.Category!
	var genre: [String]!
	var year: String!
	var tvItemDescription: String!
	var during: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.endEditing(true)
        
        navigationController?.navigationBar.barTintColor = UIColor.black

        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        
		tvItemArrayJson = try! JSONDecoder().decode([TVItem].self, from: data).sorted {$0.name < $1.name}
		
		
		sugMovies = tvItemArrayJson.filter { $0.category == TVItem.Category.movie && $0.suggested == true }
		sugSeries = tvItemArrayJson.filter { $0.category == TVItem.Category.serie && $0.suggested == true }
		topMovies = tvItemArrayJson.filter { $0.category == TVItem.Category.movie && $0.suggested == false }
		topSeries = tvItemArrayJson.filter { $0.category == TVItem.Category.serie && $0.suggested == false }
        
    }

    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.barStyle = .black
    }
    
    
    /*
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
           return 1
    }
     */
    
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            switch segmentedControl.selectedSegmentIndex
            {
                case 0:
                    return sugSeries.count
                case 1:
                    return sugMovies.count
                case 2:
                    return topSeries.count
                case 3:
                    return topMovies.count
                default:
                    break
            }
            
            return 0 //mai eseguito
    }
    
    
    //creazione delle celle
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
        cell.imageView?.clipsToBounds=true
        cell.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        

        switch segmentedControl.selectedSegmentIndex
        {
            case 0:
                
               // scrollToItem()
                    
                array=sugSeries
                titleLabel.text = "The best series for you"
            case 1:
                //scrollToItem()
                array=sugMovies
                titleLabel.text = "The best movies for you"
            case 2:
               // scrollToItem()
                array=topSeries
                titleLabel.text = "Top rated series"
            case 3:
                //scrollToItem()
                array=topMovies
                titleLabel.text = "Top rated movies"
            
            default:
                break
        }
      
        cell.backgroundColor = UIColor.green
        cell.imageView.image = UIImage(named: array[indexPath.row].imageName)
        cell.imageView.isUserInteractionEnabled = true
        cell.imageView.tag = indexPath.row

        return cell
    }
    
    
    // UICollectionView method
    func scrollToItem(){
        collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top,  animated: true)
    }

    
    //prendo le informazioni della cella per passarli alla view di info
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        imageName = array[indexPath.row].imageName
        actors = array[indexPath.row].actors
        category = array[indexPath.row].category
        name = array[indexPath.row].name
        tvItemDescription = array[indexPath.row].description
        year = array[indexPath.row].year
        //print("year1 : "+year)
        genre = array[indexPath.row].genre
        during = array[indexPath.row].during

        self.performSegue(withIdentifier: "mySegue", sender: self)
    }
       
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "mySegue"{
            if let vc = segue.destination as? InfoController {
              
               vc.name = name
               vc.category = category
               //print("year2 : "+year)
               vc.year = year
               vc.tvItemDescription = tvItemDescription
               vc.actors = actors
               vc.genre = genre
               vc.during = during
               vc.imageName = imageName
               
            
            }
        }
      }
    
    //attributi della cella
    //Controlla il size inspector della collection view lo spazio tra celle
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
       
       let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

       let numberofItem: CGFloat = 3

       let collectionViewWidth = self.collectionView.bounds.width

       let extraSpace = (numberofItem - 1) * flowLayout.minimumInteritemSpacing

       let inset = flowLayout.sectionInset.right + flowLayout.sectionInset.left

       let width = Int((collectionViewWidth - extraSpace - inset) / numberofItem)


       return CGSize(width: width, height: 220)
    }

    
    @IBAction func indexChanged(_ sender: Any) {
         collectionView.reloadData()
    }
    
    
    //Quando clicco la search bar
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //Devo deselezionarla subito altrimenti aprirò questa funzione all'infinito
        self.searchBar.endEditing(true)
        self.performSegue(withIdentifier: "mySegue2", sender: self)
    }

}

